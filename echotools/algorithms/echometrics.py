"""
Class definition(s) for Echogram objects which encapsulate their data and elementary operations on that data.

"""
import numpy as np
import pandas
import scipy.ndimage as ndi
from scipy import io
import logging
from . import utils
from typing import Union, Optional, List


class Echogram2d(object):

    def __init__(self,
                 data: Union[np.array, List],
                 depth: float,
                 index: Optional[float, List[float]] = None,
                 scale: str = 'decibel',
                 threshold: Optional[List[int]] = (0, -110),
                 bad_data=None):
        """

        :param data: Echogram data.
        :param depth:
        :param index:
        :param scale:
        :param threshold:
        :param bad_data:
        """
        self.original_data = data
        self.data = data
        self.index = index or range(self.data.shape[1])
        self.index = index
        self.bad_data = bad_data
        self.scale = scale
        self.threshold = threshold
        self.depth = np.array([depth] * self.data.shape[0]) if isinstance(depth, (int, float)) else depth.flatten()
        self.dz = abs(self.depth[1] - self.depth[0])
        self._validate_dimensionality()
        self.data = np.ma.masked_outside(self.data, threshold[0], threshold[1])

    def _validate_dimensionality(self):
        """
        Validate that the metadata products like depth are apprporiately sized.  This does not validate types first but
        maybe it should.
        :raises ValueError if some of the meta data is misaligned with respect to the echogram data.
        """
        if len(self.depth) != self.data.shape[0]:
            raise ValueError("Size of depth array does not match data.")

        if len(self.index) != self.data.shape[1]:
            raise ValueError("Size of index array does not match data.")

        if len(self.threshold) != 2:
            raise ValueError("Threshold must be sequence of length 2.")

        if not np.allclose(np.diff(self.depth), self.dz):
            raise ValueError("Depth values bins must be equally spaced.")

        if self.bad_data and self.bad_data.shape != self.data.shape:
            raise ValueError("data and bad_data must have same shape.")

    def set_scale(self, scale):
        """
        Set the scale of the echogram (decibel or linear units).

        Parameters
        ----------
        scale : string
            Scale to which the echogram will be converted.  Must be either "dB" or
            "linear".  If the echogram is already in that form, nothing is changed.
        """
        if scale == self.scale:
            logging.warning(f'Request to set scale of echogram to {scale} but it already is set.')
            return

        if scale.lower() not in ['linear', 'dB']:
            raise ValueError('Scale must be either "linear" or "dB."')

        self.scale = scale
        if scale == 'linear':
            self.data = 10 ** (self.data / 10)
            self.threshold = [10 ** (x / 10) for x in self.threshold]
        elif scale == 'dB':
            self.data = 10 * np.log10(self.data)
            self.threshold = [10 * np.log10(x) for x in self.threshold]

    def set_threshold(self, threshold):
        """

        :param threshold:
        :return:
        """
        if len(threshold) != 2:
            raise ValueError("Threshold must be a sequence of length 2.")

        self.threshold = threshold
        self.data = np.ma.masked_outside(self.data, min(threshold), max(threshold))

    def flip(self):
        self.data = np.flipud(self.data)
        self.depth = np.flipud(self.depth)

    @staticmethod
    def from_flat(file, index, depth, value, sep=',', **kwargs):
        """
        Create an echogram object from a text file in "flat" format--i.e., with
        one sample (interval and depth) per row.

        Parameters
        ----------
        file : string
            Name of file to read in.
        index : string, or list of strings
            Name or names of column(s) containing the indexing values.
        depth : string
            Name of the column containing the depth values
        value : string
            Name of the column
        sep : string
            Delimiter separating columns.  Default is a comma.
        **kwargs : additional arguments passed to Echogram()

        Returns
        -------
        An Echogram object.
        """
        data = pandas.read_table(file, sep=sep)
        idx = data[index]
        idx = [" ".join([str(x) for x in idx.ix[i]]) for i in range(data.shape[0])]
        data['index'] = idx
        data_pivot = data.pivot(depth, 'index', value)
        return Echogram(
            np.array(data_pivot), np.array(data_pivot.index).astype('float'), np.array(data_pivot.columns), **kwargs
        )

    @staticmethod
    def read_mat(file, names, **kwargs):
        """
        Create an echogram object from a .MAT file exported from Echoview.

        Parameters
        ----------
        file : string
            Name of file to read in.
        names : dictionary
            Dictionary matching the names of the fields in the .MAT file
            to the names expected by the Echogram constructor.  Must have entries
            for keys 'data', 'depth', and 'index'
        **kwargs
            Other names arguments to Echogram() (e.g. bad_data, scale, threshold).

        Returns
        -------
        An echometrics.Echogram object.
        """
        # TODO: check `names` dict for having correct keys
        mat_data = io.loadmat(file)
        return Echogram(mat_data[names['data']].T, mat_data[names['depth']], mat_data[names['index']], **kwargs)

    def remove_outliers(self, percentile, size):
        """
        Masks all data values that fall above the given percentile value
        for the area around each pixel given by size.
        """
        percentile_array = ndi.percentile_filter(self.data, percentile, size=size)
        masked_data = np.ma.masked_where(self.data > percentile_array, self.data)
        return Echogram(
            masked_data, self.depth, self.index, threshold=self.threshold, scale=self.scale, bad_data=self.bad_data
        )

    def depth_integral(self, depth_range=None, dB_result=True):
        """
        Returns the depth-integrated Sv of the echogram (Sa) as a vector.

        Parameters
        ----------
        depth_range : tuple
            maximum, minimum depths over which to integrate.
        dB_result : bool
            If true (the default), return result as backscattering strength
            (i.e. in decibel form)
        """
        depth_range = depth_range or max(self.depth), min(self.depth)
        sample = np.logical_and(min(depth_range) < self.depth, self.depth <= max(depth_range))
        integral = np.sum(utils.to_linear(self.data[sample, :]), axis=0) * self.dz
        if dB_result:
            integral = utils.to_dB(integral)
        return integral

    def sv_avg(self, depth_range=None, dB_result=True):
        """
        Returns the depth-averaged volumetric backscatter from an echogram.

        Parameters
        ----------
        depth_range : tuple
            maximum, minimum depths over which to integrate.
        dB_result : bool
            If true (the default), return result as mean volume backscattering strength
            (i.e. in decibel form)
        """
        depth_range = depth_range or max(self.depth), min(self.depth)
        sample = np.logical_and(min(depth_range) < self.depth, self.depth <= max(depth_range))
        avg = np.mean(utils.to_linear(self.data[sample, :]), axis=0)
        if dB_result:
            avg = utils.to_dB(avg)
        return avg

    def center_of_mass(self):
        """
        Returns the center of mass of an Echogram (the expected value
        of depth in each ping weighted by the sv at each depth).
        """
        linear = utils.to_linear(self.data)
        return np.ma.dot(linear.T, self.depth) / linear.sum(axis=0)

    def inertia(self):
        """
        Calculate the spread or dispersion around the center of mass.

        Parameters
        ----------
            Input Echogram object.
        """
        depth_bins, time_bins = self.data.shape
        depth_grid = np.tile(self.depth, (time_bins, 1)).T
        diff = depth_grid - self.center_of_mass.reshape(1, time_bins)
        linear = utils.to_linear(self.data)
        return np.sum((diff ** 2 * linear), axis=0) / (np.sum(linear, axis=0))

    def proportion_occupied(self):
        """
        Returns the proportion of each ping that is above the echogram's threshold.

        Parameters
        ----------
        """
        return np.sum(not self.data.mask, axis=0) / len(self.depth)

    def aggregation_index(self):
        """
        Calculate Bez and Rivoirard's (2002) Index of Aggregation.

        """
        linear = utils.to_linear(self.data)
        return np.sum(linear ** 2, axis=0) / np.sum(linear, axis=0) ** 2

    def equivalent_area(self):
        """
        Calculate the equivalent area for each ping in an echogram (the area that would
        be occupied if all cells had the same density.)
        """
        linear = utils.to_linear(self.data)
        return np.sum(linear, axis=0) ** 2 / np.sum(linear ** 2, axis=0)


def layer_mask(
        echo, gauss_size=(19, 19), med_size=(19, 19), dilate_size=(19, 19), slope_threshold=0.02, echo_threshold=-110
):
    '''
    Detect layers on an echogram.
    Returns a boolean array the same size as echo.data
    '''

    def padded_diff(a):
        empty_row = np.zeros(a.shape[1]) + np.nan
        return np.vstack((empty_row, np.diff(a, axis=0)))

    old_threshold = echo.threshold
    echo.set_threshold([echo_threshold - 1, 0])
    zerodata = np.copy(echo.data.data)
    zerodata[echo.data.mask] = min(echo.threshold)
    smoothed = ndi.gaussian_filter(zerodata, gauss_size)
    d1 = padded_diff(smoothed)
    d2 = padded_diff(d1)
    level = ((np.absolute(d1) < slope_threshold) & (d2 < 0) & (zerodata > echo_threshold) & (echo.data.mask == False))
    level = ndi.grey_dilation(ndi.median_filter(level, med_size), size=dilate_size)
    level = np.ma.array(level, mask=echo.bad_data)
    echo.set_threshold(old_threshold)
    return level


def nlayers(
        echo,
        layer=None,
        gauss_size=(19, 19),
        med_size=(19, 19),
        dilate_size=(19, 19),
        slope_threshold=0.02,
        echo_threshold=-110,
):
    '''
    Counts the layers present in an echogram.

    Parameters
    ----------
    echo: Echogram object
    layer : 2-d array
        Boolean array the same size as echo.data, designating which parts of the
        echogram are considered to be in a layer.
    gauss_size, med_size, dilate_size : tuple
        Length-2 sequences giving the height and width of convolution filters
        used in the layer-detection algorithm.  All default to (19, 19).
    slope_threshold : float
        The threshold below which the first derivative of Sv with respect to depth
        is considered "flat" for the purposes of the detection algorithm.
        Defaults to 0.02.
    echo_threshold : float
        Acoustic threshold to use as the floor in the layer detection. Defaults
        to -80.
    Notes
    -----
    The returned array is set equal to nan where the entire water column is
    marked as bad data.

    Depending on the resolution of the data and the particular system, you may
    need to futz with the parameters to get good results.  For one (possibly
    excessive) approach to futzing, see:

    Urmy, S.S., Horne, J.K., and Barbee, D.H., 2012.  Measuring the vertical
    distributional variability of pelagic fauna in Monterey Bay. ICES Journal of
    Marine Science 69 (2): 184-196.

    As a general starting point, the convolution filters should be set to about
    half the width of the layer features you are interested in.  The slope
    threshold should in general be as small as it can before it starts missing
    layers.
    '''
    if layer is None:
        layer = layer_mask(echo, gauss_size, med_size, dilate_size, slope_threshold, echo_threshold)
    layer = np.where(layer, 1., 0.)
    edges = np.nansum(abs(np.diff(layer, axis=0)), axis=0)
    edges[edges % 2 == 1] += 1
    edges[np.all(layer.mask, axis=0)] = np.nan
    return edges / 2


def calc_metrics(echo, metric_funcs):
    '''
    Applies multiple metric functions to an echogram, returning all values in an
    array with the same index as the echogram.

    Parameters
    ----------
    echo : Echogram object
        Echogram from which the metrics are calculated.
    metric_funcs : list
        List of functions to apply to the echogram.

    Returns
    -------
    A pandas DataFrame holding the metrics, with the same index as the echogram.

    Notes
    -----
    This function currently can't do either of the layer-detection functions.
    '''
    metrics = np.array([f(echo) for f in metric_funcs])
    metrics = pandas.DataFrame(metrics.T)
    metrics.index = echo.index
    metrics.columns = [f.func_name for f in metric_funcs]
    return metrics

def to_linear(x):
    """
    Utility function to convert values in linear units to decibels (10 * log10(x))

    Parameters
    ----------
    x : array_like
        Input data.

    Examples
    --------
    to_linear(-10) --> 0.1
    to_linear(-50) --> 1.0e-5
    """
    return 10 ** (x / 10.0)


def to_dB(x):
    """
    Utility to covert linear values to decibels (10**(x / 10.))

    Parameters
    ----------
    x : array_like
        Input data
    Examples
    --------
    to_dB(0.1) --> -10.0
    to_db(1.0e-5) --> -50.0
    """
    return 10 * np.log10(x)
