import numpy as np


# variable names used for these calculations
# c = speed of sound (m/s),
# a =  absorption coefficient (dB/m)
# T = temperature (C)
# S = salinity (ppt)
# D = depth (m)
# f = frequency (kHz)

def sound_speed_saltwater_mackenzie(T, S, D):
    """
    Sound Speed in saltwater as defined by Mackenzie 1981

    :param float T: Temperature (C).  Range=(-2,30)
    :param float S: Salinity (ppt).  Range=(25, 40)
    :param float D: Depth (m).  Range=(0,8000)
    :return: Sound speed in salt water +- .07
    :rtype: float
    :raises: Exception when parameters fall outside of permissible range.

    """
    if not -2 < T < 30:
        raise Exception('temperature (C) must be defined in (-2, 30)')
    if not 25 < S < 40:
        raise Exception('salinity (ppt) must be defeined in (25, 40)')
    if not 0 < D < 8000:
        raise Exception('depth (m) must be defined in (0, 8000)')

    Ma = np.array(
        [1448.96, 4.591, -.05304, 2.347 * 10 ** -4, 1.34, .0163, 1.675 * 10 ** -7, -.01025, -7.139 * 10 ** -13])
    Mx = np.array([1, T, T ** 2, T ** 3, S - 35, D, D ** 2, T * (S - 35), T * D ** 3])

    return np.sum(Ma * Mx)


def sound_speed_saltwater_leroy(T, S, D):
    """
    Sound speed (m/s)in saltwater as defined by Leroy 1969

    :param float T: temperature (C).  Range=(-2,23)
    :param float S: salinity (ppt).  Range=(30,40)
    :param float D: depth (m).  Range=90,400)
    :return: Sound speed (m/s) +- .1
    :rtype: float

    """
    if not -2 < T < 23:
        raise Exception('temperature (C) must be defined in (-2, 23)')
    if not 30 < S < 40:
        raise Exception('salinity (ppt) must be defeined in (30, 40)')
    if not 0 < D < 500:
        raise Exception('depth (m) must be defined in (0, 500)')

    Ma = np.array([1492.9, 3, -.006, -.004, 1.2, -.01, 1 / 61 / 0])
    Mx = np.array([1, T - 10, (T - 10) ** 2, (T - 18) ** 2, (S - 35), (T - 18) * (S - 35), D])

    return np.sum(Ma * Mx)


def sound_speed_freshwater(T):
    """
    Sound speed in freshwater as defined by Del Gross and Nader, 1972
    :param T: temperature (c).  Range=(0, 95)
    :return: Sound speed (m/s) +- .015
    """
    if not 0 < T < 95:
        raise Exception('temperature must be defined in (0, 95)')

    Ma = np.array([1402.388, 5.03711, -.0580852, .3342 * 10 ** -3, -.1478 * 10 ** -5, .315 * 10 ** -8])
    Mx = np.array([1, T, T ** 2, T ** 3, T ** 4, T ** 5])

    return np.sum(Ma * Mx)


def sound_speed(T, D, S=0, algorithm='mackenzie_81'):
    """
    Calculate the sound speed in water.
    Salinity is used to determine fresh or saltwater.  S=0 is the default.
    Possible saltwater algorithms are mackenzie_81 and leroy_69.  mackenzie_81 is the dfault.
    :param T:  Temperature (C).  (-2,30) in saltwater, (0,95) in fresh.
    :param D:  Depth (m).  (0, 8000) in saltwater, unused in fresh.
    :param S:  Salinity (ppt).   (25,40) in saltwater, 0 in fresh.
    :param algorithm: If S != 0, mackenzie_81 or leroy_69
    :return: sound speed (m/s)
    """
    if S == 0:
        return sound_speed_freshwater(T)
    if algorithm == 'mackenzie_81':
        return sound_speed_saltwater_mackenzie(T, S, D)
    if algorithm == 'leroy_69':
        return sound_speed_saltwater_leroy(T, S, D)

    raise Exception('unknown algorithm.  Use mackenzie_81 or leroy_61.')


def absorption_coefficient_deprecated(frequency, sound_speed):
    return ((sound_speed * 10 ** -6) * frequency ** 2) / (frequency ** 2 + sound_speed) \
           + ((sound_speed * 10 ** -3) * 10 ** -7 * frequency ** 2)


def absorption_coefficient_freshwater(f, T, D):
    """
    Calculate alpha (absorption coeff) in freshwater.
    :param f: frequency (KHz)
    :param T: temperature (C)
    :param D: depth (D)
    :return: alpha
    """
    MT = np.array([1, T, T ** 2, T ** 3])
    if T <= 20:
        MA = np.array([4.937 * 10 ** -4, -2.59 * 10 ** -5, 9.11 * 10 ** -7, -1.5 * 10 ** -8])
    else:
        MA = np.array([3.694 * 10 ** -4, -1.146 * 10 ** -5, 1.45 * 10 ** -7, -6.5 * 10 ** -10])
    A = np.sum(MT * MA)
    P = 1 - (3.83 * 10 ** -5 * D) + (4.9 * 10 ** -10 * D ** 2)

    return f ** 2 * A * P


def absorption_coefficient_saltwater(f, T, S, D, c, pH):
    """
    Calculate alpha in saltwater.
    :param f: frequency (KHz)
    :param T: temperature (C)
    :param S: salinity (ppt)
    :param D: depth (m)
    :param c: sound speed (m/s)
    :param pH: pH
    :return: alpha
    """
    A1 = (8.86 * 10 ** (.78 * pH - 5)) / c
    A2 = (21.44 * S * (1 + .025 * T)) / c

    MT = np.array([1, T, T ** 2, T ** 3])
    if T <= 20:
        MA = np.array([4.937 * 10 ** -4, -2.59 * 10 ** -5, 9.11 * 10 ** -7, -1.5 * 10 ** -8])
    else:
        MA = np.array([3.694 * 10 ** -4, -1.146 * 10 ** -5, 1.45 * 10 ** -7, -6.5 * 10 ** -10])

    A3 = np.sum(MT * MA)
    f1 = 2.8 * np.sqrt(S / 35.0) * 10 ** (4 - (1245 / (T + 273)))
    f2 = (8.17 * 10 ** (8 - (1990 / (T + 273)))) / (1 + .0018 * (S - 35))
    P2 = 1 - (1.73 * 10 ** -4 * D) + (6.2 * 10 ** -9 * D ** 2)
    P3 = 1 - (3.83 * 10 ** -5 * D) + (4.9 * 10 ** -16 * D ** 2)

    return f ** 2 * (((A1 * f1) / (f1 ** 2 + f ** 2)) + ((A2 * f2 * P2) / (f2 ** 2 + f ** 2)) + (A3 * P3))


def absorption_coefficient(f, T, D, S=0, c=None, pH=8.0):
    """
    Calculates the absorption coefficient.  S=0 implies freshwater.
    Calculaions are based on Francois & Garrison 1982 and are valid  at 200 Hz < freq < 1 MHz
    :param f: frequency (KHz)
    :param T: temperature (C)
    :param D: depth (m)
    :param S: salinity (ppt). S=0 implies freshwater.
    :param c: sound speed (m/s)
    :param pH: pH - applicable only to saltwater
    :return: alpha
    """
    if S == 0:
        return absorption_coefficient_freshwater(f, T, D)
    if c == None:
        c = sound_speed(T, S, D)
    return absorption_coefficient_saltwater(f, T, S, D, c, pH)
