import numpy as np
import scipy.signal as sig


def find_targets_in_power(ping, prominence=1, target_strength_threshold=-8, pulse_len_bounds=(.7, 1.5)):
    """
    Given a single beam in terms of power, return a list of targets
    :param samples: sample_packet namedtuple defined in readers.ex60.py
            However, anything containing sound_velocity, pulse_length, sample_interval, power and
            absorption_coeff will be sufficient.
    :param prominence:  The power level at which we bound the peak width in dB re m**2
    :param target_strength_threshold44444: the minimum dB re m**2 acceptable for a target
    :param pulse_len_bounds: A list-like defining the min and max width of a target pulse.
            It is expressed as the bounds of the ratio target pulse / transmit pulse
    :return: a list of [target_range, target_strength] values.
    """

    pulse_length_m = ping.sound_velocity * ping.pulse_length
    dR = ping.sound_velocity * ping.sample_interval
    peaks = sig.argrelextrema(np.array(ping.samples), np.greater_equal)
    candidates = []  # elements of list([x1, x2])
    for p in peaks[0]:
        threshold = ping.samples[p] - prominence
        try:
            l_bound = p - next(i for (i, s) in enumerate(ping.samples[p:0:-1]) if s < threshold)
        except:
            l_bound = p
        try:
            r_bound = p + next(i for (i, s) in enumerate(ping.samples[p:]) if s < threshold)
        except:
            r_bound = p

        # check pulse width and target strength
        normalized_pulse = ((r_bound - l_bound) * dR) / pulse_length_m

        if pulse_len_bounds[0] <= normalized_pulse <= pulse_len_bounds[1]:
            target_ranges = np.arange(start=l_bound, stop=r_bound, step=1) * dR
            target_powers = np.array(ping.samples[l_bound:r_bound])
            target_range = np.sum(target_ranges * target_powers) / np.sum(target_powers)
            # do I really want to adjust range by cT/4?  EV claims they do but that would be for recv

            # target_strength = ping.samples[p] + 40 * np.log10(
            #    target_range) + 2 * ping.absorption_coeff * target_range
            if ping.samples[p] > target_strength_threshold:
                candidates.append([ping.samples[p], target_range, l_bound, r_bound])

    # remove overlaps keeping the highest TS
    targets = []
    while candidates:
        group = [c for c in candidates if c[2] <= candidates[0][3]]
        targets.append(max(group))  # note: works because TS is the first value
        candidates = [c for c in candidates if not group.__contains__(c)]
        print candidates
    return targets
