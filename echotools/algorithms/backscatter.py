import numpy as np

np.warnings.filterwarnings('ignore')


def power_to_sv(configuration, ping_data, compressed=False, linear=False):
    """
    Converts an mxn matrix of power data to sv data based on Simrads published algorithms.
    :rtype: 2d numpy array-like
    :param configuration: ek60 CON0 packet expressed as a dict.
    :param ping_data: dict-type with the following keys: frequency, sample_interval, absorption_coeff, transmit_power,
    pulse_length, samples
    :param compressed: True if the sample data is compressed by Simrad. Default=False
    :param linear: True if the sample data is linear rather than dB.  Default=False
    :return: mxn array the same size as ping_data.samples but with the values expressing Sv.
    """
    f = ping_data.frequency
    c = ping_data.sound_velocity
    t = ping_data.sample_interval
    a = ping_data.absorption_coeff
    G = configuration.gain
    phi = configuration.equivalent_beam_angle
    pt = ping_data.transmit_power
    tau = ping_data.pulse_length

    tvg_correction_factor = 2.0  # apparent magic number for simrad?
    dr = c * t / 2.0
    lam = c / f

    tau_table = np.array(configuration.pulse_lengths)
    sac_table = np.array(configuration.sa_corrections)

    try:
        tau_table_r = (np.round(tau_table, 6) * 10000).astype(int)
        tau_r = int(np.round(tau, 6) * 10000)
        idx = np.argwhere(tau_r == tau_table_r)[0, 0]
        # sac = sac_table[idx] * 2
    except:
        pass
        # sac = 0

    samples = np.array(ping_data.samples)
    range_m = np.arange(len(samples)) * dr
    range_corr = range_m - (tvg_correction_factor * dr)
    range_corr = [max(0, i) for i in range_corr]

    tvg = 20 * np.log10(range_corr)
    tvg = np.array([max(0, i) for i in tvg])
    alpha = np.array(range_corr) * 2 * a

    # CSv = 10 * np.log10((pt * (10 ** (G / 10.0)) ** 2 * lam ** 2 * c * tau * 10 ** (phi / 10)) / (32 * np.pi ** 2))
    sv_samples = samples
    if compressed:
        compression_factor = np.log10(2) / 256  # power is compressed this way via spec
        sv_samples = sv_samples * compression_factor
    sv_samples = sv_samples + tvg + alpha  # - CSv - sac
    if linear:
        sv_samples = 10 * np.log10(sv_samples)
    return ping_data._replace(samples=sv_samples.T)


def floor_sample(echogram, method='max_sv', window=20, start_offset=5):
    return np.argmax(echogram[start_offset:], axis=0) + start_offset
    # return np.argmax(echogram[start_offset:], axis=0) + start_offset


def NASC_to_Sv(NASC, thickness_mean):
    """
    Converts nautical area scattering coefficient (NASC, m**2/nmi**2) to Sv Average

    :param NASC: single value or numpy array-like NASC values
    :param thickness_mean: the average 'thickness' of a ping defined as the extent of the region  measured. Example: \
            ping_thickness = sample_thickness * num_samples (where samples are the subset we are calculating on) \
            thickness mean = sum(ping_thickness in set of pings to analyze) / num_pings
    :return: Sv Mean

    """
    nautical_sq_mi = 1852.0
    NASC = NASC / (4.0 * np.PI * nautical_sq_mi ** 2 * thickness_mean)
    return np.log10(NASC) * 10.0


def Sv_to_NASC(Sv_mean, thickness_mean):
    """
    Converts Sv (Scattering Volume) to NASC (nautical area scattering coeff, m^2 / nmi*2)

    :param Sv_mean: single value or numpy array-like Sv values
    :param thickness_mean:  the average 'thickness' of a ping defined as the extent of the region  measured. \
    Example: ping_thickness = sample_thickness * num_samples (where samples are the subset we are calculating on) \
             thickness mean = sum(ping_thickness in set of pings to analyze) / num_pings
    :return: NASC, m^2 / nmi*2

    """
    nautical_sq_mi = 1852.0
    ABC = 10 ** (Sv_mean / 10.0) * thickness_mean
    return 4 * np.PI * nautical_sq_mi ** 2 * ABC


def background_removal(backscatter, frequency, min_range, max_range, pulse_len, absorption_coeff, sound_speed,
                       sample_bin, vertical_overlap, ping_bin, horizontal_overlap):
    # TODO: Implement Overlapping bins
    num_samples, num_pings = backscatter.shape
    np.seterr(divide='ignore')  # we will get errors from log10(range=0) and we just reset that loss to 0

    meters_per_sample = (max_range - min_range) / num_samples

    # calculate the loss matrix
    # range_vector = np.linspace(min_range, max_range, num_samples)
    range_vector = np.linspace(0, (max_range - min_range), num_samples)
    r_tvg = range_vector - (pulse_len * sound_speed / 4)  # robertis (3) <-- EK
    _loss_matrix = 20 * np.log10(r_tvg) + (2 * absorption_coeff * r_tvg)
    _loss_matrix = np.array([max(0, i) for i in _loss_matrix])
    loss_matrix = np.array([_loss_matrix] * ping_bin).T

    sv_corrected = []
    noise_scales = []
    for ping in range(0, num_pings, ping_bin):
        bs = backscatter[:, ping:ping + ping_bin]
        if bs.shape[1] != ping_bin:  # the last few pings
            loss_matrix = np.array([_loss_matrix] * bs.shape[1]).T

        power_calij = bs - loss_matrix  # robertis (2)

        bin_size = int(np.round(sample_bin / meters_per_sample))
        bins = num_samples / bin_size

        power_calij_bar = []
        for bin in range(bins):  # robertis (4) Average the cell / find the minimum
            power_calij_bar.append(
                np.mean(10 * np.log10(10 ** (power_calij[bin * bin_size:bin * bin_size + bin_size] / 10.0))))

        noise = np.min(power_calij_bar)  # robertis (5)
        noise_max = -125  # db
        noise = min(noise, noise_max)  # robert (6)

        sv_noise = np.full((bs.shape[1], num_samples), noise).T + loss_matrix  # robertis (7)
        sv = 10 * np.log10((10 ** (bs / 10.0)) - (10 ** (sv_noise / 10.0)))  # robertis (8)

        # snr = sv_corrected - sv_noise  # robertis (9)
        sv_corrected.append(sv)
        noise_scales.append(noise)

    svc = sv_corrected[0]
    sv_corrected = sv_corrected[1:]
    for s in sv_corrected:
        svc = np.append(svc, s, axis=1)

    return noise_scales, svc
