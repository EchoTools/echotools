#!/bin/python
import mmap
import xml.etree.ElementTree as et
from struct import Struct, unpack_from

filter_datagram = Struct('4sh2s128s2h')


class EKRawReader:
    def __init__(self, fileset):
        self.filename = fileset
        self.file_map = None
        self.con0 = None  # should be the first packet read; we want to store it as meta data

        # TODO: Add time & channel filters

    def packets(self):
        file_handle = open(self.filename, 'rb')
        self.file_map = mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ)
        while self.file_map.tell() < self.file_map.size():
            length = unpack_from('i', self.file_map.read(4))[0]
            data = self.file_map.read(length)
            # either xml or binary datagram
            if data[:3] == 'XML':
                xml_packet = et.fromstring(data[11:])  # skips 12 byte header b/w from XML0 to <?xml ... >

                # Parse Configuratin Packet
                if xml_packet.tag == 'Configuration':
                    configuration = {}
                    configuration['header'] = xml_packet.iterfind('Header')
                    trs = xml_packet.iterfind('Transceivers')
                    configuration['Transceivers'] = {}
                    for tr in trs:
                        configuration['Tranceivers'][tr.tag] = tr.attrib
                        configuration['Tranceivers'][tr.tag]['Channels'] = {}
                        channels = tr.find('Channels')
                        for channel in channels:
                            configuration['Tranceivers'][tr.tag]['Channels'][
                                channel.attrib['ChannelID']] = channel.attrib
                            transducers = channel.find('Transducer')
                            for transducer in transducers:
                                configuration['Tranceivers'][tr.tag]['Channels'][channel.attrib]['ChannelID'][
                                    'Transducer'] = transducer.attrib

                # Parse Environment Packet
                elif xml_packet.tag == 'Environment':
                    pass


            elif data[:3] == 'FIL':
                v = unpack_from(filter_datagram, data)
                k = ['header', 'stage', 'spare', 'channel_id', 'num_coefficients', 'decimation_factor']
                filter = dict(zip(k, v))
                coefficients = unpack_from('f' * 2 * filter['num_coefficients'], data[filter_datagram.size:])
                filter['coefficients'] = coefficients


            elif data[:3] == 'RAW':
                pass

        self.file_map.close()
        file_handle.close()
        raise StopIteration()
