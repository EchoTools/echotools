#!/usr/bin/python

import mmap
import os
from collections import namedtuple
from struct import Struct


def read_EK500DepthTG(file_map, length):
    format = Struct('2sc8scffif')
    packet = namedtuple('simrad_ek500_depth', ['channel', 'sep1', 'time', 'sep2', 'depth', 'Ss', 'Transducer', 'dummy'])
    return packet._make(format.unpack_from(file_map.read(format.size)))


def readEK500EchoTrace(file_map, length):
    data = file_map.read(length)
    format = Struct('2sc8sch')
    packet = namedtuple('simrad_ek500_depth', ['channel', 'sep1', 'time', 'sep2', 'num_detections'])
    data = packet._make(format.unpack_from(data))

    return data
    detections = []
    format = Struct('hhh')
    packet = namedtuple('simrad_ek500_detection', ['depth', 'compensated_TS', 'athwart_angle'])
    if data.num_detections == 0:
        file_map.read(2)  # always has at least a 0 detection
    else:
        for i in range(0, data.num_detections):
            detections.append(packet._make(format.unpack_from(file_map.read(format.size))))

    return data, detections


def read_EK500Echogram(file_map):
    format = Struct('c8sc4fi2fi')
    packet = namedtuple('simrad_ek500_Q', ['channel', 'sep1', 'time', 'sep2', 'tvg', 'depth', 'main_range_start',
                                           'main_range_stop', 'echogram_values', 'bottom_range_start',
                                           'bottom_range_stop', 'bottom_values'])
    data = packet._make(format.unpack_from(file_map.read(format.size)))
    format = Struct('h' * data.echogram_values)
    eg = format.unpack_from(file_map.read(format.size))
    format = Struct('h' * data.bottom_values)
    bot_eg = format.unpack_from(file_map.read(format.size))
    return data, eg, bot_eg


def read_GL(file_map, length):
    return file_map.read(length).split(',')


def read_VesselLog(file_map, length):
    log = file_map.read(length).split(',')
    # print log


def read_ErrorMessage(file_map, length):
    log = file_map.read(length).split(',')


def read_ParameterRequestReponse(file_map, length):
    log = file_map.read(length).split(',')


def read_CommentReport(file_map, length):
    log = file_map.read(length).split(',')


_packet_types = ['M','G', 'D', 'Q', 'E', 'B', 'W', 'V', 'S', 'P', 'C', 'g', 'd', 'q', 'e', 'b', 'w', 'v', 's', 'p', 'c']
TelegramHeader = Struct('hc')
tg_header_packet = namedtuple('datagram_header', ['length', 'type'])
QPacket = Struct('2sc8sci3fi2fi')

ek500_q = namedtuple('simrad_ek500_Q', ['channel', 'sep1', 'time', 'sep2', 'tvg', 'depth', 'main_range_start',
                                        'main_range_stop', 'echogram_values', 'bottom_range_start',
                                        'bottom_range_stop', 'bottom_values'])

ek500_sample = namedtuple('simrad_ek500_sample', ['channel', 'sep1', 'time', 'sep2', 'tvg', 'depth', 'main_range_start',
                                                  'main_range_stop', 'echogram_values', 'bottom_range_start',
                                                  'bottom_range_stop', 'bottom_values', 'echogram_samples',
                                                  'bottom_samples'])

ek500_echo_trace = namedtuple('simrad_ek500_echotrace', ['depth', 'compensated_TS', 'athwart_angle'])

parameters = {'frequency': {'38 kHz': 37.878, '120 kHz': 119.047}, 'pulse width': {'38 kHz': {}, '120 kHz': {}}}
parameters['pulse width']['38 kHz']['Short'] = .3
parameters['pulse width']['38 kHz']['Medium'] = 1.0
parameters['pulse width']['38 kHz']['Long'] = 3.0
parameters['pulse width']['120 kHz']['Short'] = .1
parameters['pulse width']['120 kHz']['Medium'] = .3
parameters['pulse width']['120 kHz']['Long'] = 1.0


def read_EK500_setup(filename):
    """
    read an ek500 configuration text document
    :param filename: text document containing ek500 configuration
    :return: a dictionary of values
    """

    def Tree():
        import collections
        return collections.defaultdict(Tree)

    f = open(filename)
    t = Tree()
    for line in iter(f):
        if len(line) == 0:
            continue
        temp = t
        line, value = line.strip()[1:].split('=')
        fields = line.split('/')
        for field in fields[:-1]:
            temp = temp[field]
        temp[fields[-1]] = value
    f.close()
    return t


def get_timestamps(fileset, packet_types):
    """
    Memmap through a series of files to find information related to the time stamps and locations of packets.
    :param fileset: iterable of filenames
    :param packet_types: iterable of packets types listed in _packet_types
    :return: dictionary of lists containing time stamp related values [file, time, dwell, file offset]
    """
    from collections import defaultdict
    timestamps = defaultdict(list)
    for filename in fileset:
        with open(filename) as file_handle:
            file_map = mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ)
        while file_map.tell() < file_map.size():
            tell = file_map.tell()
            tg_header = tg_header_packet._make(TelegramHeader.unpack_from(file_map.read(TelegramHeader.size)))
            if tg_header.type in packet_types:
                if tg_header.type == 'Q':
                    file_map.seek(-1, os.SEEK_CUR)
                    packet = ek500_q._make(QPacket.unpack_from(file_map.read(QPacket.size)))
                    # dwell = -1
                    # if len(timestamps[packet.channel]) > 0:
                    #    dwell = int(packet.time) - int(timestamps[packet.channel][-1][1])
                    timestamps[packet.channel].append(packet.time)
                    file_map.seek((packet.echogram_values + packet.bottom_values) * 2, os.SEEK_CUR)
            else:
                if tg_header.type not in _packet_types:
                    print 'unknown packet type:', tg_header.type
                file_map.seek(tg_header.length - 1, os.SEEK_CUR)
        file_map.close()
    return timestamps


class EK5Reader:
    def __init__(self, fileset):
        self.filename = fileset
        self.file_map = None
        # TODO: Add time & channel filters

    def packets(self):
        file_handle = open(self.filename)
        self.file_map = mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ)
        while self.file_map.tell() < self.file_map.size():
            tg_header = tg_header_packet._make(TelegramHeader.unpack_from(self.file_map.read(TelegramHeader.size)))
            if tg_header.type in _packet_types:
                if tg_header.type == 'Q':
                    packet = self._read_Q(tg_header)
                    yield packet
                else:
                    self.file_map.read(tg_header.length - 1)
            else:
                print 'unknown type:', tg_header
                #self.file_map.read(tg_header.length - 1)
                break

        self.file_map.close()
        file_handle.close()
        raise StopIteration()

    def _read_Q(self, tg_header):
        # 'c' at the start of a struct format implies 4 bytes minimum, so this hack ...
        self.file_map.seek(-1, os.SEEK_CUR)
        q_packet = list(QPacket.unpack_from(self.file_map.read(QPacket.size)))
        import struct
        samples = struct.unpack_from('h' * q_packet[8], self.file_map.read(q_packet[8] * 2))
        bot_samples = struct.unpack_from('h' * q_packet[11], self.file_map.read(q_packet[11] * 2))

        q_packet.extend([samples])
        q_packet.extend([bot_samples])

        return ek500_sample(*q_packet)
