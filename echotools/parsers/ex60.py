#!/bin/python
import mmap
import os
from collections import namedtuple
from struct import Struct, unpack
import datetime

compression_const = .011758984205624266
Configuration = Struct('128s128s128s30s98sI')
ConfigurationTransducer = Struct('128sI15f5f8s5f8s5f8s16s28s')
DatagramHeader = Struct('I4sQ')
SampleDatagram = Struct('2h12f2h2f2I')

dg_header_packet = namedtuple('datagram_header',
                              ['length', 'type', 'time_stamp'])
configuration_packet = namedtuple('configuration',
                                  ['time_stamp', 'survey', 'transect', 'sounder', 'version', 'spare', 'sounder_count',
                                   'transducers'])
transducer_packet = namedtuple('transducer',
                               ['channel_id', 'beam_type', 'frequency', 'gain', 'equivalent_beam_angle',
                                'beam_width_along', 'beam_width_athwart', 'angle_sensitivity_along',
                                'angle_sensitivity_athwart', 'angle_offset_along', 'angle_offset_athwart',
                                'pox_x', 'pox_y', 'pos_z', 'dir_x', 'dir_y', 'dir_z', 'pulse_lengths', 'spare_1',
                                'gains', 'spare_2', 'sa_corrections', 'spare_3', 'GPT_version', 'spare_4'])
sample_packet = namedtuple('sample',
                           ['time_stamp', 'channel', 'mode', 'transducer_depth', 'frequency', 'transmit_power',
                            'pulse_length', 'band_width', 'sample_interval', 'sound_velocity', 'absorption_coeff',
                            'heave', 'tx_roll', 'tx_pitch', 'temperature', 'spare_1', 'spare_2', 'rx_roll', 'rx_pitch',
                            'offset', 'num_samples', 'samples', 'angles'])

nmea_packet = namedtuple('nmea', ['time_stamp', 'text'])
annotation_packet = namedtuple('annotation', ['time_stamp', 'tag'])
packet_types = ['CON0', 'RAW0', 'NME0', 'TAG0']


def ex60time_to_datetime(timestamp):
    """
    Converts an encoded ex60 timestamp to the local date corresponding to the POSIX timestamp,
    :param timestamp: Encoded ex60 time stamp recieved in a sample packet
    :return: POSIX timestamp
    """
    try:
        ep1 = datetime.datetime(1601, 1, 1)
        ep2 = datetime.datetime(1970, 1, 1)
        return datetime.datetime.fromtimestamp((timestamp * 10.0 ** -7) - (ep2 - ep1).total_seconds())
    except ValueError as e:
        print 'Error in ex60time():', e.message

class EX60StreamReader:
    def __init__(self, stream):
        pass


class EKRawReader:
    """
    EKRawReader DocString
    """
    def __init__(self, fileset):
        """
        test
        :param fileset:
        """
        self.filename = fileset
        self.file_map = None
        self.con0 = None  # should be the first packet read; we want to store it as meta data

        # TODO: Add time & channel filters

    def __del__(self):
        try:
            self.file_map.close()
        except:
            pass

    def packets(self):
        """
        packets
        """
        file_handle = open(self.filename)
        # with mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ) as file_map:

        self.file_map = mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ)
        file_handle.close()
        while self.file_map.tell() < self.file_map.size():
            dg_header = dg_header_packet._make(DatagramHeader.unpack_from(self.file_map.read(DatagramHeader.size)))
            if dg_header.type in packet_types:
                if dg_header.type == 'CON0':
                    packet = self._read_con0(dg_header)
                    self.con0 = packet
                elif dg_header.type == 'RAW0':
                    packet = self._read_sample(dg_header)
                elif dg_header.type == 'NME0':
                    self.file_map.seek(dg_header.length - 8, os.SEEK_CUR)
                    continue
                elif dg_header.type == 'TAG0':
                    self.file_map.seek(dg_header.length - 8, os.SEEK_CUR)
                    continue
                else:
                    print 'ERROR reading file.'
                    break
                self.file_map.seek(4, os.SEEK_CUR)
                yield packet
        self.file_map.close()
        raise StopIteration()

    def _read_sample(self, dg_header):
        samples = SampleDatagram.unpack_from(self.file_map.read(SampleDatagram.size))
        power = []
        angles = []
        num_samples = samples[-1]
        mode = samples[1]
        if mode & 1 << 1:
            power = unpack('h' * num_samples, self.file_map.read(num_samples * 2))
            # power = np.array(power) * np.log10(2) / 256.0
        if mode & 1 << 0:
            angles = unpack('h' * num_samples, self.file_map.read(num_samples * 2))

        packet = [dg_header.time_stamp]
        packet.extend(samples)
        packet.extend([power])
        packet.extend([angles])

        return sample_packet(*packet)

    def _read_con0(self, dg_header):
        # (dg_header) -> dg_header_packet
        """
        Creates a namedtuple consisting of the datagram header, configuration and sounder configuration.
        There should be 1 con0 datagram in a raw file and it should be at the beginning of the file and so
        it represents useful meta-data for us to store.

        Parameters
        ----------
        header is the datagram header as a named tuple.

        Returns
        -------
        configuration_packet
        """

        con0_packet = list(Configuration.unpack_from(self.file_map.read(Configuration.size)))
        for idx in range(5):
            con0_packet[idx] = con0_packet[idx].partition('\x00')[0]
        transducer_packets = []
        for x in range(con0_packet[-1]):
            # there are 3 implied 'tables' + a 'spare' we want to arrange as single properties in the sounder tuple
            # TODO: perhaps we should bind beam widths, sensitivity, offset, dir, and pos as well.
            packet = list(ConfigurationTransducer.unpack_from(self.file_map.read(ConfigurationTransducer.size)))
            for idx in [0, 22, 28, 34, 35, 36]:
                packet[idx] = packet[idx].partition('\x00')[0]
            sounder = packet[:17]
            sounder.extend([packet[17:22], packet[22], packet[23:28], packet[28], packet[29:34], packet[34], packet[35],
                            packet[36]])
            transducer_packets.append(transducer_packet(*sounder))

        packet = [dg_header.time_stamp]
        packet.extend(con0_packet)
        # noinspection PyTypeChecker
        packet.extend([transducer_packets])
        return configuration_packet(*packet)
