#!/usr/bin/env python
"""
This file contains the data structure and parsing code for the Simrad M3 multi-beam echosounder.
"""
import mmap
from glob import glob
from struct import *

packet_header = Struct('6H11I')
data_header = Struct('12IfI4f2H1025fHHIii4H3I3f192chhff12fiddf4c3f12fII32s32sf3872c')
packet_footer = Struct('11I')
data_sample = Struct('2f')  # IQ;


class M3FormatError(Exception):

    def __init__(self, message):
        self.message = message


class M3ParamError(Exception):

    def __init__(self, message):
        self.message = 'invalid arguments: ' + message


class IMBReader:

    def __init__(self, fileset):
        self.file_names = [f'{fileset}.imb'] + glob(fileset + "['.'][0-9][0-9][0-9]")
        self._file_names = iter(self.file_names)

    def __iter__(self):
        return self

    def next(self):
        for _file in self.file_names:
            file_handle = open(_file)
            file_map = mmap.mmap(file_handle.fileno(), 0, access=mmap.ACCESS_READ)
            while file_map.tell() < file_map.size():
                buf = file_map.read(packet_header.size + data_header.size)
                packet = M3Packet.to_dict(buf)
                data_size = int(packet['num_beams'] * packet['num_samples'] * 8)  # complex float32s
                buf = file_map.read(data_size + packet_footer.size)
                packet['data'] = unpack('f' * (data_size // 4), buf[:data_size])
                footer = packet_footer.unpack_from(buf[data_size:])
                if footer[0] != packet['packet_size']:
                    raise M3FormatError(f'{_file}: Header and footer disagree on packet size')

                yield packet


class M3Packet:
    sync_word = 0x8000

    def __init__(self):
        pass

    @staticmethod
    def to_dict(packet_buffer):
        header = packet_header.unpack_from(packet_buffer[:packet_header.size])
        syncs = header[0:4]
        for sync in syncs:
            if sync != M3Packet.sync_word:
                raise M3FormatError('Invalid sync words.  Expected 0x8000 but found {sync}')

        data_hdr = data_header.unpack_from(packet_buffer[packet_header.size:])
        packet = {
            'data_type': header[5],
            'packet_size': header[16],
            'version': data_hdr[0],
            'sonarID': data_hdr[1],
            'sonarInfo': data_hdr[2:10],
            'time_sec': data_hdr[10],
            'time_msec': data_hdr[11],
            'soundspeed': data_hdr[12],
            'num_samples': data_hdr[13],
            'near_range': data_hdr[14],
            'far_range': data_hdr[15],
            'swst': data_hdr[16],
            'swl': data_hdr[17],
            'num_beams': data_hdr[18],
            'reserved_1': data_hdr[19],
            'beam_list': data_hdr[20:20 + data_hdr[18]],
            'img_sample_interval': data_hdr[1044],
            'img_destination': data_hdr[1045],
            'reserved_2': data_hdr[1046],
            'mode_id': data_hdr[1047],
            'num_hybrid_pri': data_hdr[1048],
            'hybrid_index': data_hdr[1049],
            'phase_seq_len': data_hdr[1050],
            'phase_seq_idx': data_hdr[1051],
            'num_images': data_hdr[1052],
            'sub_image_index': data_hdr[1053],
            'sonar_freq': data_hdr[1054],
            'pulse_len': data_hdr[1055],
            'ping_num': data_hdr[1056],
            'rx_filter_bw': data_hdr[1057],
            'rx_nominal_res': data_hdr[1058],
            'pulse_rep_freq': data_hdr[1059],
            'app_name': data_hdr[1060],  # :1188],
            'tx_pulse_name': data_hdr[1188],
            'tvg': data_hdr[1252:1256],
            'compass_heading': data_hdr[1256],
            'magnetic_variation': data_hdr[1257],
            'pitch': data_hdr[1258],
            'roll': data_hdr[1259],
            'depth': data_hdr[1260],
            'temperature': data_hdr[1261],
            'xoffset': data_hdr[1262],
            'yoffset': data_hdr[1263],
            'zoffset': data_hdr[1264],
            'xrotoffset': data_hdr[1265],
            'yrotoffset': data_hdr[1266],
            'zrotoffset': data_hdr[1267],
            'mounting': data_hdr[1268],
            'latitude': data_hdr[1269],
            'longitude': data_hdr[1270],
            'txwst': data_hdr[1271],
            'head_sensor_version': data_hdr[1272],
            'head_hw_status': data_hdr[1273],
            'byreserved_1': data_hdr[1274],
            'byreserved_2': data_hdr[1275],
            'internal_sensor_heading': data_hdr[1276],
            'internal_sensor_pitch': data_hdr[1277],
            'internal_sensor_roll': data_hdr[1278],
            'axes_rotator_offset_1': data_hdr[1279:1283],
            'axes_rotator_offset_2': data_hdr[1283:1287],
            'axes_rotator_offset_3': data_hdr[1287:1291],
            'start_element': data_hdr[1291],
            'end_element': data_hdr[1292],
            'custom_text_1': data_hdr[1293],
            'custom_text_2': data_hdr[1325],
            'local_time_offset': data_hdr[1357],
        }
        return packet
