""" DO NOT USE THIS CODE - IT IS FROM A PROOF OF CONCEPT  AND NEEDS REWORK """

#!/usr/bin/env python

import os
import socket
from EchoTools import ipc
from EchoTools.ipc import router
from EchoTools.parsers.simrad import m3
from struct import *
import EchoTools
import logging
import sys

if __name__ == '__main__':
    # sys.setdefaultencoding("ISO-8859-1")
    EchoTools.init_log()
    #topics = EchoTools.load_topics()
    #if not topics or 'm3_imb_raw' not in topics:
    #    logging.error('Could not acquire topic ids.  Exiting.')
    #    exit(-1)

    host = os.getenv('M3_ACQUIRE_HOST', 'localhost')
    port =  int(os.getenv('M3_ACQUIRE_PORT', 20001))
    io_port = int(os.getenv('ET_IO_PORT', 5000))
    imb_topic = 104 #topics['m3_imb_raw']
    hdr_size = m3.packet_header.size + m3.data_header.size

    rtr = router.Router(hostname='localhost', port=io_port)
    logging.info('Connecting to %s:%d' % (host, port))
    s = socket.create_connection((host, port))
    continue_processing = True
    while continue_processing:

        to_read = hdr_size
        buf = bytearray(to_read)
        view = memoryview(buf)
        while to_read: # This loop reads the sample data
            nbytes = s.recv_into(view, to_read)
            view = view[nbytes:]  # slicing views is cheap
            to_read -= nbytes

        packet = m3.M3Packet.to_dict(buf)
        data_size = int(packet['num_beams'] * packet['num_samples'] * 8)  # complex float32s

        to_read = data_size
        buf = bytearray(to_read)
        view = memoryview(buf)
        while to_read: # This loop reads the sample data
            nbytes = s.recv_into(view, to_read)
            view = view[nbytes:]  # slicing views is cheap
            to_read -= nbytes

        packet['data'] = unpack('f' * (data_size / 4), buf)
        footer = m3.packet_footer.unpack_from(s.recv(m3.packet_footer.size))
        if footer[0] != packet['packet_size']:
            logging.info('header and footer disagree on packet size')

        rtr.send(104, packet)
