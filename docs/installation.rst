.. highlight:: shell

======================================
EchoTools Installation
======================================

.. _stash repo: https://gitlab.com/EchoTools/EchoTools.git
.. code-block:: console

    $ git clone https://gitlab.com/EchoTools/EchoTools.git
    $ python setup.py install

